import axios from 'axios';

const instance = axios.create(
  {
    baseURL: 'http://exploit-server.telecomsante.loc',
    withCredentials: true,
  },
);

instance.interceptors.request.use(request =>
  // Console.log('Starting Request', JSON.stringify(request, null, 2));
  request,
);

instance.interceptors.response.use(response =>
  // Console.log('Reveiving data', JSON.stringify(response, null, 2));
  response,
);

export default instance;
