import * as actions from '../actions/socketActions.js';
import {setConnected, setDisconnected} from '../actions/establishmentsActions.js';
import {
  WS_CONNECT,
  WS_CONNECTING,
  WS_CONNECTED,
  WS_DISCONNECT,
  WS_DISCONNECTED,
  WS_SEND,
  WS_YOU_ARE,
  WS_WHO_ARE_YOU,
  WS_YOU_ARE_NOT,
  WS_ESTABLISHMENT_CONNECTED,
  WS_ESTABLISHMENT_DISCONNECTED,
} from '../utils/constants.js';

const socketMiddleware = () => {
  let socket = null;
  let socketId = null;

  const socketClose = () => {
    if (socket) {
      socket.close();
    }

    socket = null;
    socketId = null;
  };

  const onOpen = store => event => {
    console.log('Websocket open', event.target.url);
    store.dispatch(actions.wsConnected(event.target.url));
  };

  const onClose = store => () => {
    console.log('Websocket closed');
    store.dispatch(actions.wsDisconnected());
  };

  const onMessage = store => event => {
    const payload = JSON.parse(event.data);
    console.log('Receiving server message', payload);
    const userState = store.getState().userReducer;

    const user = userState.loggedIn && userState.user;
    if (!user) {
      console.log('No user loggued in, closing the Websocket connection');
      socketClose();
    }

    switch (payload.message) {
      case WS_WHO_ARE_YOU:
        socket.send(JSON.stringify({message: 'I am user', data: {login: user.username}}));
        break;
      case WS_YOU_ARE:
        socketId = payload.data;
        break;
      case WS_YOU_ARE_NOT:
        socketClose();
        break;
      case WS_ESTABLISHMENT_CONNECTED:
        store.dispatch(setConnected(payload.data));
        break;
      case WS_ESTABLISHMENT_DISCONNECTED:
        store.dispatch(setDisconnected(payload.data));
        break;
      default:
        console.log('Don\'t know what to do with this shit');
        break;
    }
  };

  return store => next => action => {
    switch (action.type) {
      case WS_CONNECT:
        if (socket) {
          socketClose();
        }

        // Connect to the remote host
        socket = new WebSocket(action.host);
        // Websocket handlers
        socket.onmessage = onMessage(store);
        socket.onclose = onClose(store);
        socket.addEventListener('open', onOpen(store));
        break;
      case WS_CONNECTING: // Not used for now
        break;
      case WS_CONNECTED: // Not used for now
        break;
      case WS_DISCONNECT:
        break;
      case WS_DISCONNECTED:
        break;
      case WS_SEND:
        break;
      default:
        console.log('the next action:', action);
        return next(action);
    }
  };
};

export default socketMiddleware();
