import React from 'react';
import ReactDOM from 'react-dom';
import '@fontsource/roboto';
import './index.scss';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/index.js';
import App from './components/App.js';
import wesbsocketMiddleware from './middlewares/wesbsocketMiddleware.js';

const store = createStore(rootReducer, applyMiddleware(thunk, wesbsocketMiddleware));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App/>
    </Provider>
  </React.StrictMode>,
  document.querySelector('#root'),
);
