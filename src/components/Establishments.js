import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Alert} from '@material-ui/lab';
import {Box} from '@material-ui/core';
import {loadEstablishments} from '../actions/establishmentsActions.js';
import Establishment from './Establishment.js';

const EstablishmentList = ({establishments}) => establishments.map(establishment => {
  const status = establishment.connected ? 'connected' : 'disconnected';
  return (
    <Establishment
      key={`${status}${establishment.name}`}
      establishment={establishment}
      status={status}
    />
  );
});

const Establishments = props => {
  const {loadEstablishments} = props;

  useEffect(() => {
    loadEstablishments();
  }, [loadEstablishments]);

  const EstablishmentCards = () => {
    if (!props.establishments.all || props.establishments.all.length === 0) {
      return <Alert severity="warning">No Establishments configured</Alert>;
    }

    return (
      <Box display="flex" flexDirection="column" height="100%">
        <EstablishmentList establishments={props.establishments.connected}/>
        <EstablishmentList establishments={props.establishments.disconnected}/>
      </Box>
    );
  };

  return <EstablishmentCards/>;
};

Establishments.propTypes = {
  establishments: PropTypes.shape({
    all: PropTypes.array.isRequired,
    connected: PropTypes.array.isRequired,
    disconnected: PropTypes.array.isRequired,
  }),
  loadEstablishments: PropTypes.func,
};

const mapStateToProps = state => {
  const {connected, disconnected, all} = state.establishmentsReducer;
  const establishments = {connected, disconnected, all};
  return {establishments};
};

const mapDispatchToProps = dispatch => ({
  loadEstablishments: () => dispatch(loadEstablishments()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Establishments);
