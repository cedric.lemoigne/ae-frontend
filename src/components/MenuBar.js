import React from 'react';
import PropTypes from 'prop-types';
import {useHistory, useLocation} from 'react-router-dom';
import {connect} from 'react-redux';
import {AppBar, Toolbar, IconButton, Box, capitalize} from '@material-ui/core';
import {
  ExitToApp as ExitToAppIcon,
  VpnLock as VpnLockIcon,
  Settings as SettingsIcon,
  Home as HomeIcon,
} from '@material-ui/icons';
import {logout} from '../actions/userActions.js';
import {VPN_LIST_URL, PATH_ADMIN, PATH_ESTABLISHMENTS, PATH_LOGIN} from '../utils/constants.js';

const getPageTitleFromLocationPath = pathname => {
  const paths = pathname.split('/').filter(element => element);
  return paths[paths.length - 1];
};

const MenuBar = props => {
  const history = useHistory();
  const location = useLocation();

  const clickLogout = async () => {
    await props.logout();
    history.push(PATH_LOGIN);
  };

  const clickGoAdmin = () => history.push(PATH_ADMIN);
  const clickGoHome = () => history.push(PATH_ESTABLISHMENTS);

  return (
    <AppBar position="relative">
      <Toolbar>
        <Box display="flex" width="100%" alignContent="center" alignItems="center" flexDirection="row" flexWrap="wrap">
          <Box>
            <button type="button" onClick={clickGoHome}>
              <img height="40em" src="http://exploit-server.telecomsante.loc/img/logo_hoppen_fond_blanc.png" alt="home"/>
            </button>
          </Box>
          <Box flexGrow={1} justify-content="center">
            <h3 align="center">{capitalize(getPageTitleFromLocationPath(location.pathname))}</h3>
          </Box>
          <Box>
            {
              getPageTitleFromLocationPath(location.pathname) === PATH_ESTABLISHMENTS.replace('/', '')
                ? null
                : <IconButton color="inherit" onClick={clickGoHome}><HomeIcon/></IconButton>
            }
            <IconButton color="inherit" onClick={() => window.open(VPN_LIST_URL, '_blank')}><VpnLockIcon/></IconButton>
            <IconButton color="inherit" onClick={clickGoAdmin}><SettingsIcon/></IconButton>
            <IconButton color="inherit" onClick={clickLogout}><ExitToAppIcon/></IconButton>
          </Box>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

MenuBar.propTypes = {
  logout: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(null, mapDispatchToProps)(MenuBar);
