import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import {connect} from 'react-redux';
import {Card, CardHeader, CardContent, CardActions, TextField, Button, Box} from '@material-ui/core';
import {Alert} from '@material-ui/lab';
import {PermIdentity as PermIdentityIcon, Input as InputIcon} from '@material-ui/icons';
import {login} from '../actions/userActions.js';
import {wsConnect} from '../actions/socketActions.js';

import {PATH_ESTABLISHMENTS} from '../utils/constants.js';

import './Login.scss';

const Login = props => {
  const [credentials, setCredentials] = useState({username: '', password: ''});
  const [loginFailed, setLoginFailed] = useState({state: false, message: 'default'});
  const history = useHistory();

  const submit = async _event => {
    const {loggedIn, message} = await props.login(credentials);

    if (loggedIn) {
      await props.wsConnect();
      history.push(PATH_ESTABLISHMENTS);
    } else {
      setLoginFailed({state: true, message});
    }
  };

  const handleLoginInput = event => {
    const {name, value} = event.target;
    setCredentials(previousCredentials => ({
      ...previousCredentials,
      [name]: value,
    }));
  };

  const ErrorMessage = () => {
    if (loginFailed.state) {
      return (
        <Alert severity="error"> {loginFailed.message}</Alert>
      );
    }

    return null;
  };

  return (
    <Box display="flex" justifyContent="center" alignItems="center" height="100%">
      <Card>
        <form noValidate autoComplete="off">
          <CardHeader title="Login" className="login_header"/>
          <CardContent>
            <ErrorMessage/>
            <Box display="flex" alignItems="center" className="login_content">
              <PermIdentityIcon/>
              <TextField required id="username" label="Login" name="username" onChange={handleLoginInput}/>
            </Box>
            <Box display="flex" alignItems="center" className="login_content">
              <InputIcon/>
              <TextField required id="password" label="Password" type="password" name="password" onChange={handleLoginInput}/>
            </Box>
          </CardContent>
          <CardActions>
            <Box ml="auto">
              <Button variant="contained" color="primary" onClick={submit}>Sign In</Button>
            </Box>
          </CardActions>
        </form>
      </Card>
    </Box>
  );
};

Login.propTypes = {
  login: PropTypes.func,
  wsConnect: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  login: credentials => dispatch(login(credentials)),
  wsConnect: () => dispatch(wsConnect('ws://exploit-server.telecomsante.loc/websocket')),
});

export default connect(null, mapDispatchToProps)(Login);
