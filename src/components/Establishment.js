import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardMedia, CardContent, Box} from '@material-ui/core';
import './Establishment.scss';

const Establishment = ({establishment, status}) => (
  <Box m="1em" display="flex" justifyContent="center" alignItems="center">
    <Card>
      <CardContent className={`establishment-card establishment-${status}`}>
        <Box display="flex" flexDirection="row" alignItems="center" height="100%">
          <Box key="logo" width="35%">
            <CardMedia component="img" src={establishment.logo} height="100%"/>
          </Box>
          <Box key="name" width="65%">
            <div key="name">{establishment.name}</div>
            <div key="label">{establishment.label}</div>
          </Box>
        </Box>
      </CardContent>
    </Card>
  </Box>
);

Establishment.propTypes = {
  establishment: PropTypes.object,
  status: PropTypes.string,
};

export default Establishment;
