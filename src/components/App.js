
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Box} from '@material-ui/core';

import {createTheme, ThemeProvider} from '@material-ui/core/styles/index.js';
import {PATH_ADMIN, PATH_ESTABLISHMENTS, PATH_LOGIN, PATH_ROOT} from '../utils/constants.js';
import Admin from './Admin.js';
import MenuBar from './MenuBar.js';
import Establishments from './Establishments.js';
import Login from './Login.js';
import colors from './colors.module.scss';

const theme = createTheme({
  palette: {
    primary: {
      main: colors.default_bg,
    },
  },
});

const PrivateRoute = ({isLoggedIn, component: Component, ...rest}) => (
  <Route
    {...rest}
    render={props => isLoggedIn ? (<><MenuBar/> <Component {...props}/></>) : <Redirect to={PATH_LOGIN}/>}
  />
);

PrivateRoute.propTypes = {
  isLoggedIn: PropTypes.bool,
  component: PropTypes.elementType,
};

const App = props => (
  <ThemeProvider theme={theme}>
    <Router>
      <Box display="flex" flexDirection="column" height="100vh">
        <Switch>
          <Route exact path={PATH_ROOT}>
            <Redirect to={PATH_LOGIN}/>
          </Route>
          <Route path={PATH_LOGIN}>
            <Login/>
          </Route>
          <PrivateRoute path={PATH_ESTABLISHMENTS} isLoggedIn={props.isLoggedIn} component={Establishments}/>
          <PrivateRoute path={PATH_ADMIN} isLoggedIn={props.isLoggedIn} component={Admin}/>
        </Switch>

      </Box>
    </Router>
  </ThemeProvider>
);

App.propTypes = {
  isLoggedIn: PropTypes.bool,
};

const mapStateToProps = state => ({
  isLoggedIn: state.userReducer.loggedIn,
});

export default connect(mapStateToProps, null)(App);
