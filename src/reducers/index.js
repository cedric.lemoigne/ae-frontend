import {combineReducers} from 'redux';
import userReducer from './userReducer.js';
import establishmentsReducer from './establishmentsReducer.js';

const rootReducer = combineReducers({
  userReducer,
  establishmentsReducer,
});

export default rootReducer;
