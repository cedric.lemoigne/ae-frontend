import {GET_ALL, WS_SET_CONNECTED, WS_SET_DISCONNECTED} from '../utils/constants.js';

const defaultState = {
  all: [],
  connected: [],
  disconnected: [],
};

const sortEstablishments = establishments => establishments.reduce((acc, establishment) => {
  establishment.connected ? acc.connected.push(establishment) : acc.disconnected.push(establishment);
  return acc;
}, {connected: [], disconnected: []});

const establishmentsReducer = (state = defaultState, action = {}) => {
  switch (action.type) {
    case GET_ALL:
      return {
        all: action.payload,
        ...sortEstablishments(action.payload),
      };
    case WS_SET_CONNECTED: {
      const name = action.payload.name;
      const all = state.all.map(establishment => {
        const establishmentCopy = Object.assign({}, establishment);
        if (establishment.name === name) {
          establishmentCopy.connected = true;
          return establishmentCopy;
        }

        return establishmentCopy;
      });

      return {
        all,
        ...sortEstablishments(all),
      };
    }

    case WS_SET_DISCONNECTED: {
      const name = action.payload.name;
      const all = state.all.map(establishment => {
        const establishmentCopy = Object.assign({}, establishment);
        if (establishment.name === name) {
          establishmentCopy.connected = false;
          return establishmentCopy;
        }

        return establishmentCopy;
      });

      return {
        all,
        ...sortEstablishments(all),
      };
    }

    default:
      return state;
  }
};

export default establishmentsReducer;
