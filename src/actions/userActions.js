import axios from '../utils/axios-instance.js';
import {LOGIN, LOGOUT} from '../utils/constants.js';

const setUser = payload => ({type: LOGIN, payload});

export const logout = () => ({type: LOGOUT});

export const login = ({username, password}) => async dispatch => {
  const method = 'POST';
  const url = 'remote/login';
  const data = {username, password};
  const headers = {crossDomain: true};
  try {
    await axios({method, url, headers, withCredentials: true, data});
    dispatch(setUser({username}));
    return {loggedIn: true, user: username};
  } catch (error) {
    let message;
    if (error.response && error.response.status === 401) {
      message = 'Invalid authentication';
    } else if (error.response) {
      message = error.response.data;
    } else if (error.request) {
      message = error.request;
    } else {
      message = error.message;
    }

    return {loggedIn: false, message};
  }
};
