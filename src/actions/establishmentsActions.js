import axios from '../utils/axios-instance.js';
import {GET_ALL, WS_SET_CONNECTED, WS_SET_DISCONNECTED} from '../utils/constants.js';

const protectedGetRequest = async ({dispatch, url, action}) => {
  const method = 'GET';
  const headers = {crossDomain: true};
  const withCredentials = true;

  try {
    const response = await axios({method, url, headers, withCredentials});
    dispatch(action(response.data));
    return {success: true};
  } catch (error) {
    let message;
    if (error.response && error.response.status === 401) {
      message = 'Invalid authentication';
    } else if (error.response) {
      message = error.response.data;
    } else if (error.request) {
      message = error.request;
    } else {
      message = error.message;
    }

    return {success: false, message};
  }
};

export const loadEstablishments = () => async dispatch => {
  const url = 'remote/proxy';
  const getAll = payload => ({type: GET_ALL, payload});
  const action = getAll;
  return protectedGetRequest({dispatch, url, action});
};

export const setConnected = payload => ({type: WS_SET_CONNECTED, payload});
export const setDisconnected = payload => ({type: WS_SET_DISCONNECTED, payload});
