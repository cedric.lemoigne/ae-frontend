# Admin-Exploit front-end in React

## library used
* react
* @material-ui
* react-router-dom
* xo
* axios
* sass

## TODO
- login + redux :
https://medium.com/better-programming/build-a-react-redux-app-with-javascript-web-tokens-9f2b73768e18
- fetch data:
https://medium.com/@dheerajmahra/fetch-initial-data-on-page-load-in-react-redux-application-16f4d8228543
Request to connect to the admin:

```
curl -v -X POST http://server.telecomsante.loc/remote/login\?username\=tech\&password\=tech
curl -X GET -b 'connect.sid=s%3AaT9en0vm4w9KgfgVAi2SuKenbgDCwKbv.xqHBaN2yp0IqgfKu%2BLZoDNEtt%2BLTW0FCIupRgrNBDlI' http://server.telecomsante.loc/remote/proxy/TLE
```

https://medium.com/developer-rants/session-cookies-between-express-js-and-vue-js-with-axios-98a10274fae7
- add tests : https://reactjs.org/docs/testing.html
- use jest
- use websockets
- use cypress.js

### Websocket:
* implement a middleware for redux: https://dev.to/aduranil/how-to-use-websockets-with-redux-a-step-by-step-guide-to-writing-understanding-connecting-socket-middleware-to-your-project-km3
* use a react context to share the WebSocket communication: https://www.pluralsight.com/guides/using-web-sockets-in-your-reactredux-app

#### Websocket needs:
- multiple components relies on them in the current implementations:
	* establishments page: each proxy/establishement card, display the current states of the differents agents
	* mediascreen terminals pages: all the locations and their information are requested via the websocket communication
	* in terminal page: all actions are executed via the websocket communication
	* all the screenshots are requested and recupered via the websocket communication
	* screenshots page: all the screenshots are requested via the websocket communication

## Notes:
* removing node-sass (deprecated) in favor of sass (full implementation in javascript)

### testing
- https://www.smashingmagazine.com/2020/06/practical-guide-testing-react-applications-jest/
- https://blog.sapegin.me/all/react-testing-1-best-practices/
- https://dev.to/penx/combining-storybook-cypress-and-jest-code-coverage-4pa5

#### jest
- https://jestjs.io/docs/en/getting-started
- https://blog.sapegin.me/all/react-testing-2-jest-and-enzyme/
- https://blog.sapegin.me/all/react-testing-3-jest-and-react-testing-library/

#### Cypress
- https://blog.sapegin.me/all/react-testing-4-cypress/
- https://docs.cypress.io
